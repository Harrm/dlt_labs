package main

import "testing"

func TestBlockProduction(t *testing.T) {
	babe := BlockProducer{
		Blockchain:      BlockChain{},
		TransactionPool: TransactionPool{},
		Storage: Storage{make(map[string]uint32)},
	}
	babe.GenerateGenesis()
	for i := 1; i < 10; i++ {
		alice, ok1 := babe.Storage.Balance["Alice"]
		bob, ok2 := babe.Storage.Balance["Bob"]
		if !ok1 || !ok2 {
			t.Error("Default accounts are missing")
		}
		babe.TransactionPool.transactions = append(babe.TransactionPool.transactions, Transaction{
			receiver: "Alice",
			sender:   "Bob",
			amount:   uint32(i * 100),
		})
		h, b := babe.ProduceBlock()
		if h == nil || b == nil {
			t.Error("Produced nil header or body!")
		}
		headers := &babe.Blockchain.Headers
		if (*headers)[i-1].BlockHash != (*headers)[i].ParentHash {
			t.Error("Parent hash is invalid")
		}
		new_alice := babe.Storage.Balance["Alice"]
		new_bob := babe.Storage.Balance["Bob"]
		if new_alice - alice != uint32(i*100) || bob - new_bob != uint32(i*100)  {
			t.Error("Transaction malfunctions")
		}

	}
}
