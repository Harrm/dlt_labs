# Sample Blockchain Web App
## Running
```sh
go run server.go
```

The web app runs on localhost:8080

## Description
The server produces a block from an accumulated pool of transactions every 3 seconds.
The web app allows to submit a transaction (mind that the sender should have a non-zero balance).
There are two pre-made accounts, Alice and Bob, with enough balance for testing.
Also, you can observe the blockchain and the ledger states (the balances of users are on the bottom of the page which displays the blockchain)