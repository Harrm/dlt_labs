package main

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"
)

var templates = template.Must(template.ParseFiles("static/index.html", "static/blockchain.html", "static/submit.html"))

type BlockHeader struct {
	ParentHash       string
	Number           uint32
	Timestamp        string
	TransactionsRoot string // hash
	StorageRoot string // hash
	BlockHash        string
}

type Transaction struct {
	receiver string
	sender string
	amount uint32
}

type TransactionPool struct {
	transactions []Transaction
}

type BlockBody struct {
	transactions []Transaction
}

type BlockChain struct {
	Headers []BlockHeader
	Bodies  []BlockBody
}


type Storage struct {
	Balance map[string]uint32
}

type BlockProducer struct {
	Blockchain      BlockChain
	TransactionPool TransactionPool
	Storage         Storage
}

func (babe *BlockProducer) HashBlock(header *BlockHeader, body *BlockBody) {
	storage_hash := sha256.New()
	for owner, amount := range babe.Storage.Balance {
		storage_hash.Write([]byte(owner))
		bs := make([]byte, 4)
		binary.LittleEndian.PutUint32(bs, amount)
		storage_hash.Write(bs)
	}
	hex_hash := hex.EncodeToString(storage_hash.Sum(nil))
	header.StorageRoot = hex_hash

	txs_hash := sha256.New()
	for _, tx := range body.transactions {
		txs_hash.Write([]byte(tx.sender))
		txs_hash.Write([]byte(tx.receiver))
		bs := make([]byte, 4)
		binary.LittleEndian.PutUint32(bs, tx.amount)
		txs_hash.Write(bs)
	}
	hex_hash = hex.EncodeToString(txs_hash.Sum(nil))
	header.TransactionsRoot = hex_hash

	header_hash := sha256.New()
	header_hash.Write([]byte(header.ParentHash))
	bs := make([]byte, 4)
	binary.LittleEndian.PutUint32(bs, header.Number)
	header_hash.Write(bs)
	header_hash.Write([]byte(header.Timestamp))
	header_hash.Write([]byte(header.TransactionsRoot))
	hex_hash = hex.EncodeToString(header_hash.Sum(nil))
	header.BlockHash = hex_hash
}

func (babe *BlockProducer) GenerateGenesis() (*BlockHeader, *BlockBody) {
	babe.Blockchain.Headers = make([]BlockHeader, 1)
	babe.Blockchain.Bodies = make([]BlockBody, 1)
	babe.Blockchain.Headers[0] = BlockHeader{
		ParentHash:       "0",
		Number:           0,
		Timestamp:        time.Now().String(),
		TransactionsRoot: "0",
		StorageRoot:      "0",
		BlockHash:        "0",
	}
	babe.Storage.Balance["Alice"] = 10500
	babe.Storage.Balance["Bob"] = 100500
	return &babe.Blockchain.Headers[0], &babe.Blockchain.Bodies[0]
}

func (babe *BlockProducer) ExecuteTx(tx Transaction) {
	s, ok := babe.Storage.Balance[tx.sender]
	if !ok {
		babe.Storage.Balance[tx.sender] = 0
		s = 0
	}
	_, ok = babe.Storage.Balance[tx.receiver]
	if !ok {
		babe.Storage.Balance[tx.receiver] = 0
	}
	if s < tx.amount {
		return
	}
	babe.Storage.Balance[tx.sender] -= tx.amount
	babe.Storage.Balance[tx.receiver] += tx.amount
}

func (babe *BlockProducer) ProduceBlock() (*BlockHeader, *BlockBody) {
	body := BlockBody{ transactions: babe.TransactionPool.transactions }
	babe.TransactionPool.transactions = nil
	for _, tx := range body.transactions {
		babe.ExecuteTx(tx)
	}
	header := BlockHeader{
		ParentHash: babe.Blockchain.Headers[len(babe.Blockchain.Headers) - 1].BlockHash,
		Number: uint32(len(babe.Blockchain.Headers)),
		Timestamp: time.Now().String(),
	}
	babe.HashBlock(&header, &body)
	babe.Blockchain.Headers = append(babe.Blockchain.Headers, header)
	babe.Blockchain.Bodies = append(babe.Blockchain.Bodies, body)
	return &header, &body
}

func rootHandler(w http.ResponseWriter, _ *http.Request) {
	err := templates.ExecuteTemplate(w, "index.html", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func createTxHandler(w http.ResponseWriter, _ *http.Request) {
	err := templates.ExecuteTemplate(w, "submit.html", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func submitTxHandler(w http.ResponseWriter, r *http.Request, tp *TransactionPool) {
	sender := r.FormValue("sender")
	receiver := r.FormValue("receiver")
	amount, err := strconv.ParseUint(r.FormValue("amount"), 10, 32)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	tx := Transaction{
		receiver: receiver,
		sender:   sender,
		amount:   uint32(amount),
	}
	tp.transactions = append(tp.transactions, tx)
	http.Redirect(w, r, "/create_tx", http.StatusFound)
}

func blockchainHandler(w http.ResponseWriter, _ *http.Request, babe *BlockProducer) {
	err := templates.ExecuteTemplate(w, "blockchain.html", babe)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {
	babe := BlockProducer{
		Blockchain:      BlockChain{},
		TransactionPool: TransactionPool{},
		Storage:         Storage{Balance: make(map[string]uint32)},
	}
	babe.GenerateGenesis()
	go func() {
		for {
			time.Sleep(time.Second * 3)
			h, _ := babe.ProduceBlock()
			fmt.Println("Produced block #", h.Number)
		}
	}()
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/blockchain", func(writer http.ResponseWriter, request *http.Request) {
		blockchainHandler(writer, request, &babe)
	})
	http.HandleFunc("/create_tx/", createTxHandler)
	http.HandleFunc("/submit_tx", func(writer http.ResponseWriter, request *http.Request) {
		submitTxHandler(writer, request, &babe.TransactionPool)
	})

	log.Fatal(http.ListenAndServe(":8080", nil))
}
